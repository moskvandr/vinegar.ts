export abstract class AbstractEvent<T> {

  protected listeners: Array<(data: T) => void> = [];

  public constructor(public ttl?: number) {}

  public addListener(listener: (data: T) => void) {
    if (this.listeners.indexOf(listener) === -1) {
      this.listeners.push(listener);
    }
  }

  public removeListener(listener: (data: T) => void) {
    const idx = this.listeners.indexOf(listener);
    if (idx !== -1) {
      this.listeners.splice(idx, 1);
    }
  }

  public hasListener(listener: (data: T) => void): boolean {
    return this.listeners.indexOf(listener) !== -1;
  }

}
