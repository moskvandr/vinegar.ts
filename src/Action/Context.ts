// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';

export interface ContextPropertyConfig {
  required?: boolean;
  type?: 'number' | 'string' | 'boolean' | 'object' | 'any';
  nullable?: boolean;
  init?(): void;
}

export function Context(config: ContextPropertyConfig = {}) {
  return (target: any, key: string) => {
    if (!target.hasOwnProperty('@ActionContextProperties')) {
      target['@ActionContextProperties'] = {};
    }

    target['@ActionContextProperties'][key] = {
      required: false,
      type: 'any',
      nullable: true,
      ...config,
    } as ContextPropertyConfig;
  };
}
