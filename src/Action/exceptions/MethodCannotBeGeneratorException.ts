export class MethodCannotBeGeneratorException extends Error {
  constructor(actionClassName: string, method: string) {
    super(`The ${method} method of action class ${actionClassName} cannot be a generator.`);
  }
}
