import * as parseFunction from 'parse-function';

import { MethodCannotBeGeneratorException } from './exceptions/MethodCannotBeGeneratorException';

const parser = parseFunction();

// обязателен ли контекст
// таймаут по данному методу
// не ждите ответа
// необязательное описание типов параметров (для проверки на клиенте)
export function Method() {
  return (target: any, propertyKey: string | symbol, descriptor: PropertyDescriptor) => {
    const name = propertyKey.toString();
    const methodInfo = parser.parse(target[name]);
    let error: any = null;

    if (methodInfo.isGenerator) {
      error = new MethodCannotBeGeneratorException(target.constructor.name, name);
    }

    if (!target.hasOwnProperty('@ActionMethods')) {
      target['@ActionMethods'] = {};
    }

    target['@ActionMethods'][name] = {
      info: methodInfo,
      error,
    };

  };
}
