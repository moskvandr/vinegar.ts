import { AbstractEvent } from './AbstractEvent';

export class ExclusiveActionEvent<T> extends AbstractEvent<T> {

  public emit(data: T, recipientId: string): void {
    // ok
  }

}
