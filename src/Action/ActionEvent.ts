import { AbstractEvent } from './AbstractEvent';

export class ActionEvent<T> extends AbstractEvent<T> {

  protected listeners: Array<(data: T) => void> = [];

  protected emit(data: T): void {
    // ok
  }

}
