export class ActionProcessor<T> {

  constructor(protected action: T, public readonly actionName: string) {
  }

  protected initMethods() {}

}
