import { v4 as uuid } from 'uuid';

export class CallMethodRequest<T> {
  public readonly transactionId!: string;
  public replyTo!: string;
  public timeout = 60000;

  constructor(
    public readonly actionName: string,
    public readonly method: string,
    public payload: T,
    public readonly provider: string,
    replyTo?: string,
  ) {
    this.transactionId = uuid();
    if (!replyTo) {
      this.replyTo = `response-provider-action-${actionName}-method-${method}-tid-${this.transactionId}`;
    }
  }
}
