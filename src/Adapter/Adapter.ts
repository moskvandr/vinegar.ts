export abstract class Adapter {

  public abstract async disconnect(): Promise<void>;

  public abstract async init(): Promise<void>;
  public abstract onConnected(handler: (adapter: Adapter) => void): void;
  public abstract onDisconnected(handler: (adapter: Adapter) => void): void;
  public abstract onReconnected(handler: (adapter: Adapter) => void): void;
}
