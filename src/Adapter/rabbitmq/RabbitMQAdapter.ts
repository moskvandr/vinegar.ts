import { Channel, connect, Connection, Message, Replies } from 'amqplib';
import { format, UrlObject } from 'url';

import { Adapter } from '../Adapter';
import { CallMethodRequest } from '../CallMethodRequest';
import { CallMethodResponse } from '../CallMethodResponse';

export class RabbitMQAdapter extends Adapter {

  public connection!: Connection;
  public autoReconnect = false;

  protected url!: string;
  protected onConnectedHandler?: (adapter: RabbitMQAdapter) => void;
  protected onDisconnectedHandler?: (adapter: RabbitMQAdapter) => void;
  protected onReconnectedHandler?: (adapter: RabbitMQAdapter) => void;
  protected channels = new Map<string, Channel>();
  protected handlers = new Map<string, <R>(response: CallMethodResponse<R>) => void>();

  private inited = false;

  constructor(urlOrConnection: UrlObject | string | Connection, protected socketOptions?: any) {
    super();
    if (urlOrConnection instanceof Object && urlOrConnection.hasOwnProperty('close')) {
      this.connection = urlOrConnection as Connection;
    } else {
      this.url = typeof urlOrConnection === 'string' ? urlOrConnection : format(urlOrConnection as UrlObject) ;
    }
  }

  public async init(): Promise<void> {
    if (this.inited) {
      return;
    }

    if (!this.connection) {
      this.connection = await connect(this.url, this.socketOptions);
      if (this.onConnectedHandler) {
        this.onConnectedHandler(this);
      }
    } else {
      this.autoReconnect = false;
    }
    this.initCloseHandler();
    this.inited = true;
  }

  public async once<M, R>(request: CallMethodRequest<M>): Promise<CallMethodResponse<R>> {
    const channel = await this.getChannel(request.actionName);
    return new Promise<CallMethodResponse<R>>((resolve, reject) => {
      this.handlers.set(request.transactionId, resolve);
      channel.publish(request.provider, '', this.toBuffer(request), {expiration: request.timeout});
      setTimeout(() => {
        this.handlers.delete(request.transactionId);
        reject('Time out.');
      }, request.timeout);
    });
  }

  public async getChannel(actionName: string): Promise<Channel> {
    if (!this.channels.has(actionName)) {
      const channel = await this.connection.createChannel();
      this.channels.set(actionName, channel);
      channel.on('close', () => {
        this.channels.delete(actionName);
      });
    }
    return this.channels.get(actionName)!;
  }

  // сообщение с ожиданием одного ответа
  // сообщение без одидания ответа
  // постоянное прослушивание

  public async disconnect() {
    try {
      const autoReconnectValue = this.autoReconnect;
      this.autoReconnect = false;
      await this.connection.close();
      delete this.connection;
      this.autoReconnect = autoReconnectValue;
    } catch (e) {
      console.warn(e);
    }
  }

  public onConnected(handler: (adapter: RabbitMQAdapter) => void): void {
    this.onConnectedHandler = handler;
  }

  public onDisconnected(handler: (adapter: RabbitMQAdapter) => void): void {
    this.onDisconnectedHandler = handler;
  }

  public onReconnected(handler: (adapter: RabbitMQAdapter) => void): void {
    this.onReconnectedHandler = handler;
  }

  public toBuffer(obj: any): Buffer {
    return Buffer.from(JSON.stringify(obj));
  }

  protected async reconnect(): Promise<void> {
    if (this.autoReconnect && this.url) {
      this.inited = false;
      await this.init();
    }
  }

  protected initCloseHandler() {
    this.connection.on('close', () => {
      delete this.connection;
      this.reconnect();
    });
  }
}
