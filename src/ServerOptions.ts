
import { Connection } from 'amqplib';
import { UrlObject } from 'url';

export interface ServerOptions {
  url: UrlObject | string;
  socketOptions?: any;
  connection?: Connection;
}
