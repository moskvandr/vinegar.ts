// Type definitions for parse-function 5.1.1
// TypeScript Version: 3.7

interface Options {
  [key: string]: any;
  ecmaVersion?: 2017;
  parse?(code: string, options?: Options): any;
}

interface ParseFunctionApp {
  parse(code: () => any, options?: Options): FunctionInfo;
  use(app: ParseFunctionApp): void;
}

interface FunctionInfo {
  name?: string;
  body: string;
  args: string[];
  params: string;
  defaults: {
    [argumentName: string]: string | undefined;
  };
  value: string;
  isValid: boolean;
  isArrow: boolean;
  isAsync: boolean;
  isNamed: boolean;
  isAnonymous: boolean;
  isGenerator: boolean;
  isExpression: boolean;
}

declare const parseFunction: (opts?: Options) => ParseFunctionApp;

declare module 'parse-function' {
  export = parseFunction;
}
