/* tslint:disable:no-implicit-dependencies */
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { suite, test, timeout } from 'mocha-typescript';

import { ActionProcessor } from '../../src/Action/ActionProcessor';
import { ActionClass } from '../data/ActionClass';

chai.use(chaiAsPromised);
const expect = chai.expect;

@suite('Test ActionProcessor')
class ActionProcessorSpec extends ActionProcessor<ActionClass> {

  constructor() {
    const action = new ActionClass();
    super(action, 'test');
  }

  @test('Check method list')
  private checkMethodList(): void {
    chai.expect((this.action as any)['@ActionMethods']).to.have.keys('methodAsync', 'methodSync');
  }

}
