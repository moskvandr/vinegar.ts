/* tslint:disable:no-implicit-dependencies */
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { suite, test, timeout } from 'mocha-typescript';

import { ActionClass } from '../data/ActionClass';
import { ActionWithGeneratorClass } from '../data/ActionWithGeneratorClass';

chai.use(chaiAsPromised);
const expect = chai.expect;

@suite('Test @Method decorator')
class MethodAndContextSpec {

  @test('Check ok method and context')
  private checkOk(): void {
    const action = new ActionClass() as any;
    chai.expect(action['@ActionMethods']).to.have.keys('methodAsync', 'methodSync');
    chai.expect(action['@ActionMethods'].methodAsync.error).to.be.null;
    chai.expect(action['@ActionContextProperties'].propertiOne).to.be.deep.equal({required: false, type: 'any', nullable: true});
    chai.expect(action['@ActionContextProperties'].propertiTwo).to.be.deep.equal({required: true, type: 'string', nullable: false});
  }

  @test('Check generator method')
  private checkGenerator(): void {
    const action = new ActionWithGeneratorClass() as any;
    chai.expect(action['@ActionMethods'].generator.error).to.not.be.null;
  }

}
