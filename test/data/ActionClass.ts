import { ActionEvent } from '../../src/Action/ActionEvent';
import { Context } from '../../src/Action/Context';
import { ExclusiveActionEvent } from '../../src/Action/ExclusiveActionEvent';
import { Method } from '../../src/Action/Method';

export class ActionClass {

  // общее или эксклюзивное
  // ttl
  public eventName = new ActionEvent<this>();
  public exclusiveEventName = new ExclusiveActionEvent<number>();

  @Context()
  public propertiOne!: number;

  @Context({
    required: true,
    nullable: false,
    type: 'string',
  })
  public propertiTwo!: string;

  @Method()
  public async methodAsync(x: string, y: number, f = 'true', e = {ddd: 1, ggg: 5}, z?: boolean): Promise<any> {

    return '';
  }

  @Method()
  public methodSync(x: string, y: number, f = true, e = {ddd: 1, ggg: parseInt('111', 10)}, z?: boolean): any { return ''; }
}
