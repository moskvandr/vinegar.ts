export const url = {
  protocol: 'amqp',
  slashes: true,
  auth: 'admin:admin',
  host: 'localhost',
  pathname: '/',
};
