import { Method } from '../../src/Action/Method';

export class ActionWithGeneratorClass {

  @Method()
  public *generator(x: string, y: number, f = 'true', e = {ddd: 1, ggg: 5}, z?: boolean): any {
    return '';
  }

}
