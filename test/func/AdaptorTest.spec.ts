/* tslint:disable:no-implicit-dependencies */
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { suite, test, timeout } from 'mocha-typescript';

import { Server } from '../../src';
import { Adapter } from '../../src/Adapter/Adapter';
import { RabbitMQAdapter } from '../../src/Adapter/rabbitmq/RabbitMQAdapter';
import { url } from '../data/mqUrl';

chai.use(chaiAsPromised);
const expect = chai.expect;

@suite('Adapter')
class AdaptorTestSpec {

  private static server: Server;
  private static adapter: Adapter;

  @test('Connect and disconnect')
  private async connectAndDisconnect(): Promise<void> {
    const adapter = new RabbitMQAdapter(url);
    await adapter.init();
    const server = new Server(AdaptorTestSpec.adapter);
    await adapter.disconnect();
  }

}
